<?php get_header(); ?>

<div id="main-content">
	<div class="container full-width">
		<div id="content-area" class="clearfix">
		
		<?php $catdesc = category_description(); if($catdesc){echo '<div class="category-description">'. $catdesc.'</div>';} ?>
		
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
					<div class="one_third">
						<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'archive-single-post',array( 'class' => 'archive-single-post' )); ?></a>
					</div>
					<div class="two_third et_column_last" style="padding-top:44px;">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php the_excerpt(); ?>
						<a class="blog-readmore" href="<?php the_permalink(); ?>"><?php echo esc_html__('Read More...','mogel'); ?></a>
					</div>
					<div class="clearfix"></div>
					</article> <!-- .et_pb_post -->
					
			<?php
					endwhile;

					if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?>

		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
