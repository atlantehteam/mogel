<?php
get_header();
?>
<h1 class="page-post-title" style="margin-top:20px"><?php echo esc_html__('Oops','mogel');?><span class="title-logo"></span></h1>
<div id="main-content" class="notfound-page">
	<div class="container full-width">
		<div id="content-area" class="clearfix">
		<div class="notfound">
			<p><?php echo esc_html__('Something went wrong','mogel');?><br />
			<?php echo esc_html__("We're working on it","mogel");?></p>
		<a href="<?php echo get_home_url(); ?>"><?php echo esc_html__("Back to homepage","mogel"); ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home.png" alt="Home" /></a>
		</div>
			
		</div> <!-- #content-area -->
	</div> <!-- .container -->
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/404bg.png" alt="404" style="margin-top:10px;width:100%;" />

</div> <!-- #main-content -->

<?php
get_footer();