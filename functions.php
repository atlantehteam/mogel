<?php 
// APPLY STYLESHEET FOR CHILD THEME
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' ); 
function theme_enqueue_styles() { 
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
}
/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
// LOAD JS
function add_theme_scripts() {
wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/custom.js', array ( 'jquery' ), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
// IMAGE SIZES
add_image_size( 'testimonial-slider', 133, 133, true );
add_image_size( 'archive-single-post', 458, 304, true );
add_image_size( 'related-post', 423, 290, true );
// ACF OPTIONS PAGE
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
// REGISTER TESTIMONIAL POST TYPE
function wpdocs_codex_testimonial_init() {
    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post type general name', 'mogel' ),
        'singular_name'         => _x( 'Testimonial', 'Post type singular name', 'mogel' ),
        'menu_name'             => _x( 'Testimonials', 'Admin Menu text', 'mogel' ),
        'name_admin_bar'        => _x( 'Testimonial', 'Add New on Toolbar', 'mogel' ),
        'add_new'               => __( 'Add New', 'mogel' ),
        'add_new_item'          => __( 'Add New Testimonial', 'mogel' ),
        'new_item'              => __( 'New Testimonial', 'mogel' ),
        'edit_item'             => __( 'Edit Testimonial', 'mogel' ),
        'view_item'             => __( 'View Testimonial', 'mogel' ),
        'all_items'             => __( 'All Testimonials', 'mogel' ),
        'search_items'          => __( 'Search Testimonials', 'mogel' ),
        'parent_item_colon'     => __( 'Parent Testimonials:', 'mogel' ),
        'not_found'             => __( 'No testimonials found.', 'mogel' ),
        'not_found_in_trash'    => __( 'No testimonials found in Trash.', 'mogel' )
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'testimonial' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor','thumbnail' ),
    );
 
    register_post_type( 'testimonial', $args );
}
add_action( 'init', 'wpdocs_codex_testimonial_init' );
// SHORTCODE TO DISPLAY TAX RETURNS INFORMATION ON HOMEPAGE
add_shortcode( 'display_tax_returns', 'display_tax_returns_function' );
function display_tax_returns_function( $atts ) {
    ob_start();
	
	if( have_rows('tax_returns') ):
		$count=0;while ( have_rows('tax_returns') ) : the_row();$count++;
		$title = get_sub_field('info_title');
		$text = get_sub_field('info_text');
		$image = get_sub_field('info_image');
		?>
        <div class="one_fourth<?php if($count % 4 == 0){echo ' et_column_last';} ?>">
			<div class="et_pb_module et_pb_toggle et_pb_toggle_6 et_pb_toggle_item et_pb_toggle_close">
				<h5 class="et_pb_toggle_title"><?php echo $title; ?></h5>				
				<div class="et_pb_toggle_content clearfix">
				<?php echo $text; ?>
				</div>
				<span class="toggle_img_wrap et_pb_toggle_title"><img src="<?php echo $image; ?>" alt="<?php $title; ?>" /></span>
			</div>
			
		</div>
		<?php if($count % 4 == 0){echo '<div class="clearfix"></div>';} ?>
    <?php 
	endwhile;
	else :
		// no rows found
	endif;
	$myvariable = ob_get_clean();
    return $myvariable; 
}
// SHORTCODE TO DISPLAY TESTIMONIALS ON HOMEPAGE
add_shortcode( 'testimonial-posts-list', 'testimonial_listing_shortcode' );
function testimonial_listing_shortcode( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'testimonial',
        'posts_per_page' => '6',
        'order' => 'DESC'
    ) );
    if ( $query->have_posts() ) { ?>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <div class="testimonial-post-list slider">
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <div class="homepage-testimonials-item">
			<?php $refund = get_field('refund'); ?>
			<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'testimonial-slider',array( 'class' => 'personimg aligncenter' )); ?></a>
                <a class="personname" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<p class="refund"><?php echo esc_html__('Refund: ','mogel') . $refund; ?></p>
				<div class="testimonial-content"><?php the_content(); ?></div>
            </div>
            <?php endwhile; ?>
		</div>
            <?php wp_reset_postdata(); ?>
 
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/js/slick-theme.css"/>
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<script type="text/javascript">
		 $('.testimonial-post-list.slider').slick({
		  arrows: true,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  adaptiveHeight: true,
		  rtl: true,
		  autoplay: true,
		  autoplaySpeed: 4000,
			responsive: [
				{
				  breakpoint: 768,
				  settings: {
					arrows: false,
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 654,
				  settings: {
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			  ]		  
		});
	  </script>
		<div class="clearfix"></div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
// DISABLE COMMENTS SITEWIDE
// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');

// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');
// REMOVE PROJECTS POST TYPE
add_filter( 'et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1 );
function mytheme_et_project_posttype_args( $args ) {
	return array_merge( $args, array(
		'public'              => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => false
	));
}

function wpf_dev_datetime_date_dropdowns( $args, $form_id, $field ) {
 
    $form_id = (int) $form_id;
 
    // Only apply if the form's ID is 10
    if ( $form_id === 1348 ) {
 
        // Set the lower and upper limits for each date dropdown
        $args['months'] = range( 01, 12 ); 
        $args['days'] = range( 01, 31 ); 
        $args['years'] = range( 2002, 1920 ); 
 
    }
 
    return $args;
}
add_filter( 'wpforms_datetime_date_dropdowns', 'wpf_dev_datetime_date_dropdowns', 1348, 3 );
